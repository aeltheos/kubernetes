{
  description = "Aeltheos kubernetes ressources";

  inputs = {
    kubenix.url = "github:hall/kubenix";
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
  };
  outputs = { self, kubenix, flake-utils, nixpkgs }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };
    in
    {
      formatter = pkgs.nixpkgs-fmt;

      devShells.default = pkgs.mkShell {
        buildInputs = [ pkgs.jq pkgs.yq ];
      };

      packages = {
        csi-provisioner = import ./packages/csi-provisioner.nix { inherit pkgs; };
        csi-attacher = import ./packages/csi-attacher.nix { inherit pkgs; };
        csi-snapshotter = import ./packages/csi-snapshotter.nix { inherit pkgs; };
        csi-resizer = import ./packages/csi-resizer.nix { inherit pkgs; };
        cephcsi = import ./packages/cephcsi.nix { inherit pkgs; };
        rbd-deploy = import ./packages/rbd-deploy.nix {inherit pkgs; };
      };
    });
}
