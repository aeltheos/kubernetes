{ pkgs, ... }:
let
  binary = pkgs.stdenv.mkDerivation rec {
    pname = "csi-attacher";
    version = "v4.3.0";

    src = pkgs.fetchFromGitHub {
      owner = "kubernetes-csi";
      repo = "external-attacher";
      fetchSubmodules = true;
      rev = version;
      sha256 = "sha256-K7OiT3wL8xEbXvpnmEgiUyNq2I/OioWYif/Bcg7w5nw=";
    };
    
    buildInputs = with pkgs; [
      go
    ];

    configurePhase = ''
      runHook preConfigure
      export GOCACHE=$TMPDIR/go-cache
      export HOPATH=$TMPDIR/go
      runHook postConfigure
    '';

    patchPhase = ''
      runHook prePatchPhase
      patchShebangs --build release-tools/*
      runHook postPatchPhase
    '';

    installPhase = ''
      runHook preInstallPhase
      mkdir -p $out/bin
      cp bin/csi-attacher $out/bin/csi-attacher
      runHook postInstallPhase
    '';
  };

in
pkgs.dockerTools.buildImage {
  name = "csi-attacher-3.4.0";

  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [ binary ];
    pathsToLink = [ "/bin" ];
  };
}
