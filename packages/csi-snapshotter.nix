{ pkgs, ... }:
let
  binary = pkgs.stdenv.mkDerivation rec {
    pname = "csi-snapshotter";
    version = "v6.2.2";

    src = pkgs.fetchFromGitHub {
      owner = "kubernetes-csi";
      repo = "external-snapshotter";
      fetchSubmodules = true;
      rev = version;
      sha256 = "sha256-bB3LMZea/JGMCUBCm9nrXIvsGzux1Bxfd9dLnfabrtg=";
    };
    
    buildInputs = with pkgs; [
      go
    ];

    configurePhase = ''
      runHook preConfigure
      export GOCACHE=$TMPDIR/go-cache
      export HOPATH=$TMPDIR/go
      runHook postConfigure
    '';

    patchPhase = ''
      runHook prePatchPhase
      patchShebangs --build release-tools/*
      runHook postPatchPhase
    '';

    installPhase = ''
      runHook preInstallPhase
      mkdir -p $out/bin
      cp bin/csi-snapshotter $out/bin/csi-snapshotter
      runHook postInstallPhase
    '';
  };

in
pkgs.dockerTools.buildImage {
  name = "csi-snapshotter-6.2.2";

  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [ binary ];
    pathsToLink = [ "/bin" ];
  };
}
