{ pkgs, namespace ? "kube-system", monitors ? [], clusterID ? "", ... }:
let
  cephcsi = import ./cephcsi.nix {inherit pkgs;};
  csi-attacher = import ./csi-attacher.nix {inherit pkgs;};
  csi-provisioner = import ./csi-provisioner.nix {inherit pkgs;};
  csi-resizer = import ./csi-resizer.nix {inherit pkgs;};
  csi-snapshotter = import ./csi-snapshotter.nix {inherit pkgs;};
  
  ceph-csi-config = pkgs.writeText "ceph-csi-config.yaml" ''
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: ceph-csi-config
      namespace: ${namespace}
    data:
      config.json: |-
        [
          {
            "clusterID": "${clusterID}",
            "monitors": ${builtins.toJSON monitors}
          }
        ]
  '';

  ceph-csi-encryption-kms-config = pkgs.writeText "ceph-csi-encryption-kms.yaml" ''
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: ceph-csi-encryption-kms-config
      namespace: ${namespace}
    data:
      config.json: |-
        {}
  '';

  ceph-config = pkgs.writeText "ceph-config.yaml" ''
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: ceph-config
      namespace: ${namespace}
    data:
      ceph.conf: |
        [global]
        auth_cluster_required = cephx
        auth_service_required = cephx
        auth_client_required = cephx
      keyring: |
  '';

in pkgs.stdenvNoCC.mkDerivation rec {
  pname = "cephcsi-rbd-manifest";
  version = "v3.9.0";

  src = pkgs.fetchFromGitHub {
    owner = "ceph";
    repo = "ceph-csi";
    rev = version;
    sha256 = "sha256-dKn79EIveepeMzFPweQ3BE3YMCg7mj8EycMbBH8J8PQ=";
  };

  nativeBuildInputs = with pkgs; [
    kubectl
    kubectl-convert
  ];

  buildPhase = ''
    runHook preBuildPhase
    cd $src/deploy/rbd/kubernetes
    
    # copy ressources
    cp csi-nodeplugin-rbac.yaml $TMPDIR/
    cp csi-provisioner-rbac.yaml $TMPDIR/
    cp csi-rbdplugin-provisioner.yaml $TMPDIR/
    cp csi-rbdplugin.yaml $TMPDIR/
    cp csidriver.yaml $TMPDIR/

    # inject hardcoded configMap
    cp ${ceph-csi-config} $TMPDIR/ceph-csi-config.yaml
    cp ${ceph-csi-encryption-kms-config} $TMPDIR/ceph-csi-encryption-kms-config.yaml
    cp ${ceph-config} $TMPDIR/ceph-config.yaml
    
    

    cd $TMPDIR
    kubectl convert -f . --local -o json > ressources.json

    substituteInPlace ressources.json \
      --replace "quay.io/cephcsi/cephcsi:v3.9.0" "${cephcsi}" \
      --replace "registry.k8s.io/sig-storage/csi-attacher:v4.3.0" "${csi-attacher}" \
      --replace "registry.k8s.io/sig-storage/csi-provisioner:v3.5.0" "${csi-provisioner}" \
      --replace "registry.k8s.io/sig-storage/csi-resizer:v1.8.0" "${csi-resizer}" \
      --replace "registry.k8s.io/sig-storage/csi-snapshotter:v6.2.2" "${csi-snapshotter}" \
      --replace '"namespace": "default"' '"namespace": "${namespace}"'

    runHook postBuildPhase
  '';

  installPhase = ''
  cp $TMPDIR/ressources.json $out
  '';
}
