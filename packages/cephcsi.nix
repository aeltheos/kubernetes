{ pkgs, ... }:
let
  binary = pkgs.stdenv.mkDerivation rec {
    pname = "cephcsi";
    version = "v3.9.0";

    src = pkgs.fetchFromGitHub {
      owner = "ceph";
      repo = "ceph-csi";
      fetchSubmodules = true;
      rev = version;
      sha256 = "sha256-dKn79EIveepeMzFPweQ3BE3YMCg7mj8EycMbBH8J8PQ=";
    };
    buildInputs = with pkgs; [
      go
      ceph.dev
    ];

    configurePhase = ''
      runHook preConfigure
      export GOCACHE=$TMPDIR/go-cache
      export HOPATH=$TMPDIR/go
      runHook postConfigure
    '';

    installPhase = ''
      runHook preInstallPhase
      mkdir -p $out/bin
      cp _output/cephcsi $out/bin/cephcsi
      runHook postInstallPhase
    '';
  };

in
pkgs.dockerTools.buildImage {
  name = "cephcsi-3.9.0";

  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [ binary ];
    pathsToLink = [ "/bin" ];
  };
}
