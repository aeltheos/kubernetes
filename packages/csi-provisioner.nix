{ pkgs, ... }:
let
  binary = pkgs.stdenv.mkDerivation rec {
    pname = "csi-provisioner";
    version = "v3.5.0";

    src = pkgs.fetchFromGitHub {
      owner = "kubernetes-csi";
      repo = "external-provisioner";
      fetchSubmodules = true;
      rev = version;
      sha256 = "sha256-XRuDuMmcms5Qr1w+zKrizxA1woPz8GccgN3ZmSdukaY=";
    };
    
    buildInputs = with pkgs; [
      go
    ];

    configurePhase = ''
      runHook preConfigure
      export GOCACHE=$TMPDIR/go-cache
      export HOPATH=$TMPDIR/go
      runHook postConfigure
    '';

    patchPhase = ''
      runHook prePatchPhase
      patchShebangs --build release-tools/*
      runHook postPatchPhase
    '';

    installPhase = ''
      runHook preInstallPhase
      mkdir -p $out/bin
      cp bin/csi-provisioner $out/bin/csi-provisioner
      runHook postInstallPhase
    '';
  };

in
pkgs.dockerTools.buildImage {
  name = "csi-provisioner-3.5.0";

  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [ binary ];
    pathsToLink = [ "/bin" ];
  };
}
