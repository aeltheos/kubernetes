{ pkgs, ... }:
let
  binary = pkgs.stdenv.mkDerivation rec {
    pname = "csi-resizer";
    version = "v1.8.0";

    src = pkgs.fetchFromGitHub {
      owner = "kubernetes-csi";
      repo = "external-resizer";
      fetchSubmodules = true;
      rev = version;
      sha256 = "sha256-q964Hzu3Zmtih7GYgKF/1rVLoEBkasQfAdGHxc/gQEQ=";
    };
    
    buildInputs = with pkgs; [
      go
    ];

    configurePhase = ''
      runHook preConfigure
      export GOCACHE=$TMPDIR/go-cache
      export HOPATH=$TMPDIR/go
      runHook postConfigure
    '';

    patchPhase = ''
      runHook prePatchPhase
      patchShebangs --build release-tools/*
      runHook postPatchPhase
    '';

    installPhase = ''
      runHook preInstallPhase
      mkdir -p $out/bin
      cp bin/csi-resizer $out/bin/csi-resizer
      runHook postInstallPhase
    '';
  };

in
pkgs.dockerTools.buildImage {
  name = "csi-resizer-1.8.0";

  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [ binary ];
    pathsToLink = [ "/bin" ];
  };
}
